package api.tanita;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class TanitaAPITest {

	public static void main(String[] args) {
		HttpsURLConnection con = null;
		try{
			URL url = new URL("https://www.healthplanet.jp/oauth/auth");


			con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("client_id", "345.Iac2rQjxyY.apps.healthplanet.jp");
			con.setRequestProperty("redirect_uri", "https://www.healthplanet.jp/success.html");
			con.setRequestProperty("scope", "innerscan");
			con.setRequestProperty("response_type", "code");
			con.connect();
			if(con.getResponseCode() == HttpsURLConnection.HTTP_OK){
				InputStream in = con.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in));

				{
					String line;
					while((line = br.readLine()) != null){
						System.out.println(line);
					};
				}
			}else{
				System.out.println("Error");
			}
		} catch(IOException e){
			e.printStackTrace();
		} finally {
			if(con != null){
				con.disconnect();
			}
		}
	}


}
