package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class HttpConnectUtil {

	public static void main(String[] args) {

		HttpConnectUtil http = new HttpConnectUtil();
		try{
			http.setURL("http://www.google.co.jp/");
//			http.setURL("https://www.healthplanet.jp/oauth/auth");
//			http.setRequestProperty("client_id", "yyokuno");
//			http.setRequestProperty("redirect_uri", "https://www.healthplanet.jp/success.html");
//			http.setRequestProperty("scope", "innerscan");
//			http.setRequestProperty("response_type", "code");

			List<String> result = http.postRequest();
			for(String line : result){
				System.out.println(line);
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private HttpURLConnection http;
	private HttpsURLConnection https;
	private Map<String, String> map;
	private URL url;
	private List<String> requestResult;

	public HttpConnectUtil(){
	}

	public void setURL(final String url) throws MalformedURLException{
		this.url = new URL(url);
	}

	public void setRequestProperty(final String key, final String value){
		if(map == null){
			map = new HashMap<>();
		}
		map.put(key, value);
	}

	public List<String> postRequest() throws IOException{
		requestResult = httpRequest(HttpRequestType.POST);
		return requestResult;
	}

	public List<String> getRequest() throws IOException{
		requestResult = httpRequest(HttpRequestType.GET);
		return requestResult;
	}


	private List<String> httpRequest(final HttpRequestType req) throws IOException{
		List<String> requestResult = new ArrayList<>();

		try{
			http = (HttpURLConnection) url.openConnection();
			http.setRequestMethod(req.toString());
			if(map != null){
				for(String key : map.keySet()){
					http.setRequestProperty(key, map.get(key));
				}
			}

			http.connect();
			if(http.getResponseCode() == HttpURLConnection.HTTP_OK){
				InputStream in = http.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in));

				{
					String line;
					while((line = br.readLine()) != null){
						requestResult.add(line);
					};
				}
			}
		}catch(IOException e){
			e.printStackTrace();
			throw e;
		} finally {
			if(http != null){
				http.disconnect();
			}
			http = null;
			requestResult = null;
		}

		return requestResult;
	}
}
