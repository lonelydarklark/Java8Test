package http;

public enum HttpRequestType{
	GET("GET"),
	POST("POST");

	private String str;
	private HttpRequestType(String str){
		this.str = str;
	}
	public String toString(){
		return str;
	}
}