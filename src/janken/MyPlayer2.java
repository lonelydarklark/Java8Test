package janken;

import java.util.ArrayList;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.JankenUtils;
import socket.janken.common.ResultEnum;

public class MyPlayer2 implements JankenAI{

	private ArrayList<HandsEnum> hands = new ArrayList<>();
	private ArrayList<HandsEnum> enemyHands = new ArrayList<>();

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new MyPlayer2());
		client.playJankenGame();
	}

	// TODO ここで必要なメソッドを実装してください。
	// JankenUtilsクラスのstaticメソッドを使うと便利です。
	// ほしいものがあれば依頼してください。

	@Override
	public String getName() {
		// TODO プレーヤーの名前を返しましょう
		return "My Player";
	}

	@Override
	public HandsEnum getHand() {
		// TODO 勝負に出す手を返すこと。nullを返すと反則負け扱いになります。
		//      相手に勝てる手を考えさせよう。
		HandsEnum hand = JankenUtils.getRandomHand();
		hands.add(hand);

		return hand;
	}

	@Override
	public void putResult(ResultEnum result) {
		// TODO 勝負の結果が変数にセットされてきます。
		//      どう使うかはあなた次第。
		enemyHands.add(JankenUtils.getEnemyHand(hands.get(hands.size()-1), result));
	}
}
