package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CalcSleepingTime {
	private static BufferedReader br = null;
	public static void main(String[] args) throws Exception{

	    final int N = readIntLine();
	    for(int i = 0; i < N; i++){
	    	final int ZANGYO_MTIME = readIntLine();
	    	final int SLEEP_MTIME = 6 * 60 + (ZANGYO_MTIME / 3);
	    	final int TOMMOROW_7_MTIME = (24 + 7) * 60;

	    	printMinute2Time(TOMMOROW_7_MTIME - SLEEP_MTIME);
	    }

	}

	private static void printMinute2Time(final int min){
		final int HOUR = min / 60 - ((min / 60) >= 24 ? 24 : 0);
		final int MINUTE = min % 60;

		System.out.printf("%02d:%02d\n", HOUR, MINUTE);
	}

	private static int readIntLine() throws Exception{
		if(br == null){
			br = new BufferedReader(new InputStreamReader(System.in));
		}
		return Integer.parseInt(br.readLine());
	}
}
