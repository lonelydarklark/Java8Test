package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CountCat {
	public static void main(String[] args) throws Exception{
		final String CAT = "cat";
	    final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String STR = br.readLine();

	    int cnt = 0;
	    while(STR.indexOf(CAT) >= 0){
	    	cnt++;
	    	STR = STR.substring(STR.indexOf(CAT) + CAT.length(), STR.length());

	    }
	    System.out.println(cnt);
	}
}
