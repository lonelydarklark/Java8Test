package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CutCube {
    private static BufferedReader br = null;
	public static void main(String[] args) throws Exception{
		final String[] FIRST_LINE = readLineSplittedBySpace();

		final int X = Integer.parseInt(FIRST_LINE[0]);
		final int Y = Integer.parseInt(FIRST_LINE[1]);
		final int Z = Integer.parseInt(FIRST_LINE[2]);
		final int N = Integer.parseInt(FIRST_LINE[3]);

		ArrayList<Integer> x_i = new ArrayList<>();
		ArrayList<Integer> y_i = new ArrayList<>();

		// 頂点となる座標を全てArrayListにaddする
		x_i.add(0);
		y_i.add(0);
		for(int i = 0; i < N; i++){
			final String[] LINE = readLineSplittedBySpace();

			switch (LINE[0]){
				case "0":
					x_i.add(Integer.parseInt(LINE[1]));
					break;
				case "1":
					y_i.add(Integer.parseInt(LINE[1]));
					break;
			}
		}
		x_i.add(X);
		y_i.add(Y);

		final int X_MIN = calcMinDiff(x_i);
		final int Y_MIN = calcMinDiff(y_i);

		System.out.println(X_MIN * Y_MIN * Z);
	}

	private static int calcMinDiff(final List<Integer> list){
		Collections.sort(list);
		int result = list.get(list.size()-1);
		for(int i = 1; i < list.size(); i++){
			int diff = list.get(i) - list.get(i-1);
			if(diff < result){
				result = diff;
			}
		}

		return result;
	}

	private static String[] readLineSplittedBySpace() throws Exception{
		if(br == null){
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		return br.readLine().split(" ");
	}
}
