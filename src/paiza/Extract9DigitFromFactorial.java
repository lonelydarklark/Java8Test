package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Extract9DigitFromFactorial {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final int N = Integer.parseInt(br.readLine());

		long fact = factorialOf10Digit(N);

		System.out.println(fact);
	}

	private final static long factorialOf10Digit(final int n){
		long fact = 1;

		for(int i = 2; i <= n; i++){
			fact *= i;
			while(fact % 10l == 0){
				fact /= 10l;
			}
			fact %= 1000000000l;
		}

		return fact;
	}
}
