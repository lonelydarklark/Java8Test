package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MakeCatStrFromRandomCat {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final String INPUT_STR = br.readLine();

		int[] cat_cnt = new int[]{
				INPUT_STR.length() - INPUT_STR.replaceAll("c", "").length(),
				INPUT_STR.length() - INPUT_STR.replaceAll("a", "").length(),
				INPUT_STR.length() - INPUT_STR.replaceAll("t", "").length(),
		};

		final int CAN_MAX = Math.max(Math.max(cat_cnt[0], cat_cnt[1]), cat_cnt[2]);
		final int CAN_MIN = Math.min(Math.min(cat_cnt[0], cat_cnt[1]), cat_cnt[2]);
		int maked_count = 0;

		{
			while(maked_count < CAN_MIN){
				maked_count++;
			}
			cat_cnt[0] -= maked_count;
			cat_cnt[1] -= maked_count;
			cat_cnt[2] -= maked_count;
		}

		System.out.println(maked_count);
		System.out.println(CAN_MAX - maked_count - cat_cnt[0]);
		System.out.println(CAN_MAX - maked_count - cat_cnt[1]);
		System.out.println(CAN_MAX - maked_count - cat_cnt[2]);

	}
}