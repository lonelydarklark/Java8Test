package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MatrixInMatrix {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws Exception {
        // 自分の得意な言語で
        // Let's チャレンジ！！
        final int N = Integer.parseInt(br.readLine());
        final int[][] N_MATRIX = readMatrixLine(N);

        final int M = Integer.parseInt(br.readLine());
        final int[][] M_MATRIX = readMatrixLine(M);

        final int MAX_SEARCH_RANGE = N - M + 1; //調査する左上セルの範囲
        for(int i = 0; i < MAX_SEARCH_RANGE; i++){
            for(int j = 0; j < MAX_SEARCH_RANGE; j++){
                if(isSameMatrixInMatrix(N_MATRIX, i, j, M_MATRIX)){
                    System.out.println(i + " " + j);
                    return;
                }
            }
        }
        System.out.println("error");
    }

    private static int[][] readMatrixLine(final int M_SIZE) throws Exception{
        int[][] result = new int[M_SIZE][M_SIZE];
        for(int i = 0; i < M_SIZE; i++){
            int[] cols = string2intArray(br.readLine(), " ");
            result[i] = cols;
        }
        return result;
    }

    private static int[] string2intArray(final String STR, final String SEP){
    	final String[] SEPARATED_STR = STR.split(SEP);
    	int[] result = new int[SEPARATED_STR.length];

    	for(int i = 0; i < result.length; i++){
    		result[i] = Integer.parseInt(SEPARATED_STR[i]);
    	}

    	return result;
    }

    private static boolean isSameMatrixInMatrix(final int[][] N_MATRIX, final int COL, final int ROW, final int[][] M_MATRIX){
        final int MATRIX_SIZE = M_MATRIX.length;

        for(int n_i =  COL, m_i = 0; m_i < MATRIX_SIZE ; n_i++, m_i++){
            for(int n_j =  ROW, m_j = 0; m_j < MATRIX_SIZE ; n_j++, m_j++){
                if(N_MATRIX[n_i][n_j] != M_MATRIX[m_i][m_j]){
                    return false;
                }
            }
        }

        return true;
    }
}
