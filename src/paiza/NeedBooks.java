package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringJoiner;

public class NeedBooks {
	private static BufferedReader br = null;
	public static void main(String[] args) throws Exception{

		final int SHELF_SIZE = readIntLine(); // 1行目
		final int MY_SHELF_BOOKS = readIntLine(); // 2行目
		int[] buyBooks = new int[SHELF_SIZE];
		final int[] MY_BOOKS = readIntLineSplittedBySpace(); // 3行目
		for(int i = 0; i < MY_SHELF_BOOKS; i++){
			buyBooks[MY_BOOKS[i]-1] = 1;
		}

		final int STORE_SHELF_BOOKS = readIntLine(); // 4行目
		final int[] STORE_BOOKS = readIntLineSplittedBySpace(); //5行目
		for(int i = 0; i < STORE_SHELF_BOOKS; i++){
			buyBooks[STORE_BOOKS[i]-1] = buyBooks[STORE_BOOKS[i]-1] - 1;
		}

		StringJoiner result = new StringJoiner(" ");
		result.setEmptyValue("None");
		for(int i = 0; i < buyBooks.length; i++){
			if(buyBooks[i] < 0){
				result.add(""+(i+1));
			}
		}
		System.out.println(result);
	}

	private static String readLine() throws Exception{
		if(br == null){
			br = new BufferedReader(new InputStreamReader(System.in));
		}
		return br.readLine();
	}

	private static int readIntLine() throws Exception{
		return Integer.parseInt(readLine());
	}

	private static int[] readIntLineSplittedBySpace() throws Exception{
		String[] line = readLine().split(" ");
		int[] intLine = new int[line.length];

		for(int i = 0; i < intLine.length; i++){
			intLine[i] = Integer.parseInt(line[i]);
		}

		return intLine;
	}
}
