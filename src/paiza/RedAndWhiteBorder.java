package paiza;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RedAndWhiteBorder {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final int N = Integer.parseInt(br.readLine());
		final int M = Integer.parseInt(br.readLine());

		boolean isRed = true;
		for(int i = 1; i <= M; i++){
			if(isRed){
				System.out.print("R");
			}else{
				System.out.print("W");
			}

			if(i % N == 0){
				isRed = !isRed;
			}
		}
		System.out.println();
	}
}