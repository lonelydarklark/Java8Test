package rankC;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws Exception {
        // 自分の得意な言語で
        // Let's チャレンジ！！
        try(Scanner sc = new Scanner(System.in)){
        	final int N = Integer.parseInt(sc.next());
        	final int M = Integer.parseInt(sc.next());
        	final int K = Integer.parseInt(sc.next());

        	System.out.printf("%d %d %d\n", N, M, K);

        	final double[] C = new double[N];
        	for(int i = 0; i < N; i++){
        		C[i] = Double.parseDouble(sc.next());
        	}

        	for(double d : C){
        		System.out.printf("%f ", d);
        	}
        	System.out.println();

        	final Integer[] RESULT = new Integer[K];
        	for(int m_i = 0; m_i < M; m_i++){
        		double s = 0;
        		for(int n_i = 0; n_i < N; n_i++){
//        			System.out.println("m_i = " + m_i + ":" + "n_i = " + n_i);
        			s += C[n_i] * Double.parseDouble(sc.next());
        		}
        		RESULT[RESULT.length -1] = (int)s;
        		Arrays.sort(RESULT, Collections.reverseOrder());
        	}

        	for(int res : RESULT){
        		System.out.println(res);
        	}
        }
    }
}
