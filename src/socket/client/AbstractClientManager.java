package socket.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

abstract public class AbstractClientManager {

	private String host = "192.168.3.8";
	private final int PORT = 20000;
	private int timeoutMsec =  5 * 60 * 1000;

	private Socket sc;

	private boolean debugFlag;

	public AbstractClientManager(){
		this(false);
	}

	public AbstractClientManager(boolean debugFlag){
		this.debugFlag = debugFlag;
	}

	final protected void sendMessage(String str) throws IOException{
		if(this.sc == null){
			this.sc = new Socket(host, PORT);
			this.sc.setSoTimeout(timeoutMsec);
		}

		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sc.getOutputStream())));
		pw.println(str);
		pw.flush();
		if(debugFlag){
			System.out.println("Send: " + str);
		}
	}

	final protected String receiveResponse() throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(sc.getInputStream()));
		try{
			String str = br.readLine();
			if(debugFlag){
				System.out.println("Get Response: "+str);
			}

			return str;
		}catch(IOException e){
			e.printStackTrace();
			throw e;
		}finally{
			sc.close();
			sc = null;
		}
	}

	final protected void setHost(String host){
		this.host = host;
	}

	final protected String getHost(){
		return host;
	}

	final protected String getIpAddress(){
		if(sc == null){
			return null;
		}

		return sc.getInetAddress().toString().substring(1);
	}

}
