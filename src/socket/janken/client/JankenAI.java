package socket.janken.client;

import socket.janken.common.HandsEnum;
import socket.janken.common.ResultEnum;

abstract public interface JankenAI {

	/**
	 *
	 * @return プレーヤー名を設定する
	 */
	abstract public String getName();

	/**
	 *
	 * @return 出す手を返す
	 */
	abstract public HandsEnum getHand();

	/**
	 *
	 * @param result 勝負の結果が格納される
	 */
	abstract public void putResult(ResultEnum result);

}
