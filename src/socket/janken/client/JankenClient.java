package socket.janken.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import socket.client.AbstractClientManager;
import socket.janken.client.defaultPlayers.ImpressionPlayer;
import socket.janken.client.defaultPlayers.ReverseSequencePlayer;
import socket.janken.client.defaultPlayers.SequencePlayer;
import socket.janken.client.defaultPlayers.PaperPlayer;
import socket.janken.client.defaultPlayers.RandomPlayer;
import socket.janken.client.defaultPlayers.RockPlayer;
import socket.janken.client.defaultPlayers.ScissorsPlayer;
import socket.janken.client.defaultPlayers.SpecialPlayer;
import socket.janken.common.HandsEnum;
import socket.janken.common.JankenUtils;
import socket.janken.common.ResultEnum;

public class JankenClient extends AbstractClientManager{

	private boolean connectedServer;

	private int maxOfGames = -1;
	private boolean endOfGames = false;
	private JankenAI player;

	private boolean displayPlayingLog = true;

	private final Map<ResultEnum, Integer> resultSummary = new HashMap<>();
	{
		resultSummary.put(ResultEnum.WIN, 0);
		resultSummary.put(ResultEnum.LOSE, 0);
		resultSummary.put(ResultEnum.DRAW, 0);
	}

	private static final JankenAI[] ai = new JankenAI[]{
		new RockPlayer(),
		new ScissorsPlayer(),
		new PaperPlayer(),
		new RandomPlayer(),
		new SequencePlayer(),
		new ReverseSequencePlayer(),
		new ImpressionPlayer(),
		new SpecialPlayer(),
	};

	public static void main(String[] args) {

		JankenClient client = new JankenClient(true);
		client.playJankenGame();
	}

	public JankenClient(){
		this(false);
	}

	public JankenClient(boolean debugFlag){
		this(ai[JankenUtils.getRandom(0, ai.length-1)], debugFlag);
	}

	public JankenClient(JankenAI player){
		this(player, false);
	}

	public JankenClient(JankenAI player, boolean debugFlag){
		super(debugFlag);
		try{
			this.player = player;
			System.out.println(player.getName() + " play the game!");
			readHostnameFromStdin();
		}catch(IOException e){
			e.printStackTrace();
		}

	}

	public void playJankenGame(){

		connectedServer = false;

		try{

			if(!connectedServer){
				sendHandShakeProtocol();
			}

			if(connectedServer){
				System.out.println("Start to send Janken to server.");
				for(int i = 1; i <= maxOfGames; i++){
					System.out.println("Game " + i);

					ResultEnum result = sendJankenHandProtocol(); //手を送り勝敗をもらってくる

					System.out.println(result.getStr());
					player.putResult(result);

					//結果合計計算
					int sumTmp = resultSummary.get(result);
					resultSummary.put(result, ++sumTmp);

					if(endOfGames){
						System.out.println("End of Games");
					}

					try{
						Thread.sleep(100);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}

				System.out.println("Game Fin");
				if(displayPlayingLog){
					printResultSummary();
				}
				connectedServer = false;
			}

		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private void printResultSummary(){
		System.out.println("--------Result--------");
		System.out.println("Num Of Games: " + maxOfGames);
		System.out.printf("Win : %3d  %2.1f%%\n", resultSummary.get(ResultEnum.WIN), resultSummary.get(ResultEnum.WIN).intValue()*100/(double)maxOfGames);
		System.out.printf("LOSE: %3d  %2.1f%%\n", resultSummary.get(ResultEnum.LOSE), resultSummary.get(ResultEnum.LOSE).intValue()*100/(double)maxOfGames);
		System.out.printf("DRAW: %3d  %2.1f%%\n", resultSummary.get(ResultEnum.DRAW), resultSummary.get(ResultEnum.DRAW).intValue()*100/(double)maxOfGames);
	}

	public void setDisplayPlayingLog(boolean displayPlayingLog){
		this.displayPlayingLog = displayPlayingLog;
	}

	private void readHostnameFromStdin() throws IOException{
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){

			System.out.print("Type the Hostname: ");
			setHost(br.readLine());
		}finally{
		}
	}

	private void sendHandShakeProtocol() throws IOException{

		String res = null;
		try{
			sendMessage(player.getName());
			res = receiveResponse();

		}catch(IOException e){
			throw e;
		}

		System.out.println("Connected!");
		connectedServer = true;
		maxOfGames = Integer.parseInt(res.split(":")[1]);
	}

	/**
	 * 出す手をサーバに送り、勝敗を受け取る
	 * プロトコルの形式
	 * "PlayerName:[Hand]"
	 * @param sc
	 * @return 勝敗
	 * @throws IOException
	 */
	private ResultEnum sendJankenHandProtocol() throws IOException{

		HandsEnum hand = player.getHand();
		sendMessage(player.getName() + ":" + hand.getId()); //出す手を送る
		System.out.println(hand.getStr());

		String[] response = receiveResponse().split(":"); //最終ゲームのみ":End"がついて送られるので分けておく

		if(response.length > 2){
			endOfGames = true;
		}
		return ResultEnum.getFromId(response[0]);
	}
}
