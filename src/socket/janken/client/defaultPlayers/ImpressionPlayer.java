package socket.janken.client.defaultPlayers;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.JankenUtils;
import socket.janken.common.ResultEnum;

public class ImpressionPlayer implements JankenAI{

	private HandsEnum preEnemyHand;

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new ImpressionPlayer());
		client.playJankenGame();
	}

	@Override
	public HandsEnum getHand() {
		if(preEnemyHand == null){
			preEnemyHand = JankenUtils.getRandomHand();
			return preEnemyHand;
		}
		return preEnemyHand;
	}

	@Override
	public void putResult(ResultEnum result) {
		preEnemyHand = JankenUtils.getEnemyHand(preEnemyHand, result);
	}

	@Override
	public String getName() {
		return "Impression player";
	}
}
