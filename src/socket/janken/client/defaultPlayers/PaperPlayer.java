package socket.janken.client.defaultPlayers;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.ResultEnum;

public class PaperPlayer implements JankenAI{

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new PaperPlayer());
		client.playJankenGame();
	}

	@Override
	public String getName() {
		return "Paper Player";
	}

	@Override
	public HandsEnum getHand() {
		return HandsEnum.PAPER;
	}

	@Override
	public void putResult(ResultEnum result) {
	}
}
