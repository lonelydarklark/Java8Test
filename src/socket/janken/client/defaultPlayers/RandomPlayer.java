package socket.janken.client.defaultPlayers;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.JankenUtils;
import socket.janken.common.ResultEnum;

public class RandomPlayer implements JankenAI{

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new RandomPlayer());
		client.playJankenGame();
	}

	@Override
	public HandsEnum getHand() {
		return JankenUtils.getRandomHand();
	}

	@Override
	public void putResult(ResultEnum result) {
		// Do not everything.
	}

	@Override
	public String getName() {
		return "Random Player";
	}
}
