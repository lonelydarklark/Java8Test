package socket.janken.client.defaultPlayers;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.ResultEnum;

public class RockPlayer implements JankenAI{

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new RockPlayer());
		client.playJankenGame();
	}

	@Override
	public String getName() {
		return "Rock Player";
	}

	@Override
	public HandsEnum getHand() {
		return HandsEnum.ROCK;
	}

	@Override
	public void putResult(ResultEnum result) {
	}
}
