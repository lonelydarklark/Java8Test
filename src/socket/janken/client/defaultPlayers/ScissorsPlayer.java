package socket.janken.client.defaultPlayers;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.ResultEnum;

public class ScissorsPlayer implements JankenAI{

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new ScissorsPlayer());
		client.playJankenGame();
	}

	@Override
	public String getName() {
		return "Scissors Player";
	}

	@Override
	public HandsEnum getHand() {
		return HandsEnum.SCISSORS;
	}

	@Override
	public void putResult(ResultEnum result) {
	}
}
