package socket.janken.client.defaultPlayers;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.JankenUtils;
import socket.janken.common.ResultEnum;

public class SequencePlayer implements JankenAI{

	private HandsEnum preHand;

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new SequencePlayer());
		client.playJankenGame();
	}

	@Override
	public HandsEnum getHand() {
		if(preHand == null){
			preHand = JankenUtils.getRandomHand();
			return preHand;
		}
		return JankenUtils.getSequencedHand(preHand);
	}

	@Override
	public void putResult(ResultEnum result) {
	}

	@Override
	public String getName() {
		return "Sequence Player";
	}
}
