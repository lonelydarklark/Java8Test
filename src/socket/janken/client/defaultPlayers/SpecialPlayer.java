package socket.janken.client.defaultPlayers;

import janken.MyPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import socket.janken.client.JankenAI;
import socket.janken.client.JankenClient;
import socket.janken.common.HandsEnum;
import socket.janken.common.JankenUtils;
import socket.janken.common.ResultEnum;

public class SpecialPlayer implements JankenAI{

	public static void main(String[] args) {
		JankenClient client = new JankenClient(new SpecialPlayer(true), false);
		client.playJankenGame();
	}

	private int gameCnt = 0;
	private HandsEnum lastHand;

	private boolean printFlag = false;

	private final Map<HandsEnum, Integer> winRateMap = new HashMap<>();
	{
		winRateMap.put(HandsEnum.ROCK, 0);
		winRateMap.put(HandsEnum.SCISSORS, 0);
		winRateMap.put(HandsEnum.PAPER, 0);
	}

	public SpecialPlayer(){

	}

	public SpecialPlayer(boolean printFlag){
		this.printFlag = printFlag;
	}

	@Override
	public String getName() {
		return "Special AI";
	}

	@Override
	public HandsEnum getHand() {
		// TODO 自動生成されたメソッド・スタブ
		this.lastHand = getMaxWinRateHand();
		return lastHand;
	}

	@Override
	public void putResult(ResultEnum result) {
		gameCnt++;
		if(result == ResultEnum.WIN){
			int lastWins = winRateMap.get(lastHand);
			winRateMap.put(lastHand, lastWins+1);
		}

		if(printFlag){
			printPutResultLog();
		}
	}

	private void printPutResultLog(){
		System.out.println("---------GAME " + gameCnt + "---------");
		for(HandsEnum h : winRateMap.keySet()){
			System.out.println(" " + h.getStr() + ": " + winRateMap.get(h) + " wins.");
		}
		System.out.println("------------------------");
	}

	private HandsEnum getMaxWinRateHand(){
		HandsEnum hand = null;
		int max = 0;

		if(gameCnt <= 0){
			return JankenUtils.getRandomHand();
		}

		for(HandsEnum h : winRateMap.keySet()){
			int rate = winRateMap.get(h);
			if(max == rate){
				hand = null;
			}else if(max < rate){
				max = rate;
				hand = h;
			}
		}

		if(max/gameCnt < 16.7 || hand == null){
			return JankenUtils.getRandomHand();
		}
		return hand;
	}
}
