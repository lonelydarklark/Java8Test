package socket.janken.common;

public enum HandsEnum {

	ROCK("0", "グー"),
	PAPER("1", "パー"),
	SCISSORS("2", "チョキ"),
	ERROR("-1", null);

	private String id;
	private String str;

	private HandsEnum(String id, String str){
		this.id = id;
		this.str = str;
	}

	public String getId(){
		return id;
	}

	public String getStr(){
		return str;
	}

	public static HandsEnum getFromId(String id){
		for(HandsEnum he : values()){
			if(he.getId().equals(id)){
				return he;
			}
		}

		throw new IllegalArgumentException();
	}
}
