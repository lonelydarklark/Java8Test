package socket.janken.common;

public class JankenUtils {

	/**
	 * 乱数によりランダムな手を返す
	 * @return 手
	 */
	public static HandsEnum getRandomHand(){
		return HandsEnum.getFromId(String.valueOf(getRandom(0, 2)));
	}

	/**
	 * グー・チョキー・パーの順になるように、次の手を返す
	 * @param preHand 前の手
	 * @return 次の手
	 */
	public static HandsEnum getSequencedHand(HandsEnum preHand){
		switch(preHand){
		case ROCK:
			return HandsEnum.SCISSORS;
		case SCISSORS:
			return HandsEnum.PAPER;
		case PAPER:
			return HandsEnum.ROCK;
		default:
			return getRandomHand();
		}
	}

	/**
	 * パー・チョキ・グーの順になるように、次の手を返す
	 * @param preHand 前の手
	 * @return 次の手
	 */
	public static HandsEnum getReverseSequencedHand(HandsEnum preHand){
		switch(preHand){
		case ROCK:
			return HandsEnum.PAPER;
		case SCISSORS:
			return HandsEnum.ROCK;
		case PAPER:
			return HandsEnum.SCISSORS;
		default:
			return getRandomHand();
		}
	}

	/**
	 * 出した手とその対戦結果から相手の手を導く
	 * @param yourHand 出した手
	 * @param result 対戦結果
	 * @return 相手の手
	 */
	public static HandsEnum getEnemyHand(HandsEnum yourHand, ResultEnum result){
		switch(yourHand){
		case ROCK:
			switch(result){
			case WIN:
				return HandsEnum.SCISSORS;
			case LOSE:
				return HandsEnum.PAPER;
			case DRAW:
				return HandsEnum.ROCK;
			default:
					break;
			}
		case SCISSORS:
			switch(result){
			case WIN:
				return HandsEnum.PAPER;
			case LOSE:
				return HandsEnum.ROCK;
			case DRAW:
				return HandsEnum.SCISSORS;
			default:
					break;
			}
		case PAPER:
			switch(result){
			case WIN:
				return HandsEnum.ROCK;
			case LOSE:
				return HandsEnum.SCISSORS;
			case DRAW:
				return HandsEnum.PAPER;
			default:
					break;
			}
		default:
			return HandsEnum.ERROR;
		}
	}

	public static int getRandom(int min, int max){
		return (int)(Math.random() * (max - min + 1) + min);
	}

}
