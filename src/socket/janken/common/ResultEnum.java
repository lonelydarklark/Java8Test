package socket.janken.common;

public enum ResultEnum {

	WIN("2", "WIN"),
	DRAW("1", "DRAW"),
	LOSE("0", "LOSE"),
	NONE("-1", "");

	private String id;
	private String str;

	private ResultEnum(String id, String str){
		this.id = id;
		this.str = str;
	}

	public String getId(){
		return id;
	}

	public String getStr(){
		return str;
	}

	public static ResultEnum getFromId(String id){
		for(ResultEnum re : values()){
			if(re.getId().equals(id)){
				return re;
			}
		}

		throw new IllegalArgumentException();
	}
}
