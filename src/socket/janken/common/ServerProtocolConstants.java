package socket.janken.common;

public enum ServerProtocolConstants {

	ACK("JANKEN_ACK"),
	NAK("JANKEN_NAK");

	private String protocol;

	private ServerProtocolConstants(String str){
		this.protocol = str;
	}

	public String getProtocol(){
		return protocol;
	}
}
