package socket.janken.server;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import socket.janken.common.HandsEnum;
import socket.janken.common.ServerProtocolConstants;
import socket.server.AbstractServerManager;

public class JankenServer extends AbstractServerManager{

	private Referee referee;
	private int gamesCounter;
	private int timeoutCnt;
	private final int MAX_TIMEOUT_COUNT = 3;

	public static void main(String[] args) {
		try{
			new JankenServer(1000).startServer();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	public JankenServer(){
		super();
		referee = Referee.getRefereeInstance();
	}

	public JankenServer(int maxOfGames){
		this();
		referee.setMaxOfGames(maxOfGames);
	}

	/**
	 * 2スレッドで各プレーヤーの手待ちをする
	 * レフェリーはシングルトンで1人のみ。
	 */
	@Override
	protected String actionOfReceivedSocketMessage(String mes, String ipAddress) throws IOException{

		if(!hasPlayer(ipAddress, mes)){
			String res = receiveHandshakeReq(mes, ipAddress);
			if(referee.isPlayerMax()){
				gamesCounter = 0;
			}
			return res;
		}else{
			String playerName = mes.split(":")[0];
			String key = ipAddress + ":" + playerName;
			boolean waiting = receiveAndSetHand(mes, ipAddress);

			if(waiting){
				while(true){
					try{
						if(referee.ableToFight()){
							jankenFight();
						}

						if(referee.getPlayer(key) != null &&
								referee.getPlayer(key).getResult() != null){
							break;
						}

						System.out.println(key + " is waiting");
						System.out.println(referee);

						Thread.sleep(100);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}

				String response = referee.getResult(key).getId();
				if(referee.getMaxOfGames() <= gamesCounter){
					response += ":END";
					resetConnection(key);
				}
				return response;
			}else{
				return ServerProtocolConstants.NAK.getProtocol();
			}
		}
	}

	private String receiveHandshakeReq(String mes, String ipAddress) throws IOException, UnknownHostException {
		boolean couldAddPlayer = addPlayer(ipAddress, mes);

		/*
		 * 重複IPじゃなければOK　ACKと試合数を返す
		 * 重複してたらNAKを返す。
		 */
		return (couldAddPlayer)
				? ServerProtocolConstants.ACK.getProtocol() + ":" + referee.getMaxOfGames()
				: ServerProtocolConstants.NAK.getProtocol();
	}

	private boolean addPlayer(String ipAddress, String playerName){
		return referee.addPlayer(ipAddress + ":" + playerName, new Player());
	}

	private boolean receiveAndSetHand(String mes, String ipAddress){
		System.out.println("receive janken hand protocol: " + mes);
		String[] res = mes.split(":");

		return referee.setHand(ipAddress + ":" + res[0], HandsEnum.getFromId(res[1]));
	}

	private void jankenFight(){
		referee.fightAndJudge();
		gamesCounter++;
	}

	private void resetConnection(String key){
		referee.resetPlayer(key);
	}

	private boolean hasPlayer(String ipAddress, String message){
		String playerName = message.split(":")[0];

		return referee.hasPlayer(ipAddress + ":" + playerName);
	}

	@Override
	protected void actionOfCatchSocketTimeoutException(SocketTimeoutException e)
			throws SocketTimeoutException {
		if(MAX_TIMEOUT_COUNT <= timeoutCnt++){
			System.out.println("SERVER CLOSE");
			this.closeServer();
		}
		System.out.println("Timeout count: " + timeoutCnt);
	}
}
