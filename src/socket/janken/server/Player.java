package socket.janken.server;

import socket.janken.common.HandsEnum;
import socket.janken.common.ResultEnum;

public class Player{

	private HandsEnum hand;
	private ResultEnum result;
	private String name;

	public Player(){
		this(null);
	}

	public Player(String name){
		this.hand = null;
		this.result = null;
		this.name = name;
	}

	public void setHand(HandsEnum hand){
		this.hand = hand;
	}

	public void setResult(ResultEnum result){
		this.result = result;
	}

	public void setName(String name){
		this.name = name;
	}

	public HandsEnum getHand(){
		return hand;
	}

	public ResultEnum getResult(){
		return result;
	}

	public String getName(){
		return name;
	}
}
