package socket.janken.server;

import java.util.HashMap;

import socket.janken.common.HandsEnum;
import socket.janken.common.ResultEnum;


public class Referee {

	private static Referee referee;

	private HashMap<String, Player> playersMap;
	private static final int MAX_OF_PLAYERS = 2;
	private int maxOfGames = 100;

	private Referee(){
		this.playersMap = new HashMap<>();
	}

	public static Referee getRefereeInstance(){
		if(referee == null){
			referee = new Referee();
		}
		return referee;
	}

	public boolean isPlayerMax(){
		return playersMap.size() >= MAX_OF_PLAYERS;
	}

	public boolean addPlayer(String key, Player p){
		if(isPlayerMax()){
			System.out.println("2players");
			return false;
		}

		playersMap.put(key, p);
		if(playersMap.size() == 1){
			System.out.println("0 -> 1 player");
		}else if(playersMap.size() == 2){
			System.out.println("1 -> 2 players");
		}

		return true;
	}

	public boolean setHand(String key, HandsEnum hand){
		if(key == null){
			throw new IllegalArgumentException();
		}

		if(playersMap.containsKey(key)){
			Player p = playersMap.get(key);
			p.setHand(hand);
			return true;
		}
		return false;
	}

	public synchronized boolean ableToFight(){
		if(isPlayerMax()){
			for(Player p : playersMap.values()){
				if(p.getHand() == null){
					return false;
				}
			}

			return true;
		}

		return false;
	}

	public synchronized void fightAndJudge(){
		if(!ableToFight()){
			throw new IllegalArgumentException();
		}

		Player p1, p2;
		{
			Player[] p = playersMap.values().toArray(new Player[]{});
			p1 = p[0];
			p2 = p[1];
		}

		resetResults();
		switch(p1.getHand()){
		case ROCK:
			switch(p2.getHand()){
			case ROCK:
				draw(p1, p2);
				break;
			case SCISSORS:
				p1Win(p1, p2);
				break;
			case PAPER:
				p2Win(p1, p2);
				break;
			case ERROR:
				p1Win(p1, p2);
				break;
			}
			break;

		case SCISSORS:
			switch(p2.getHand()){
			case ROCK:
				p2Win(p1, p2);
				break;
			case SCISSORS:
				draw(p1, p2);
				break;
			case PAPER:
				p1Win(p1, p2);
				break;
			case ERROR:
				p1Win(p1, p2);
				break;
			}
			break;

		case PAPER:
			switch(p2.getHand()){
			case ROCK:
				p1Win(p1, p2);
				break;
			case SCISSORS:
				p2Win(p1, p2);
				break;
			case PAPER:
				draw(p1, p2);
				break;
			case ERROR:
				p1Win(p1, p2);
				break;
			}
			break;

		case ERROR:
			switch(p2.getHand()){
			case ERROR:
				draw(p1, p2);
				break;
			default:
				p2Win(p1, p2);
				break;
			}
		}

		resetHands();
	}

	private void p1Win(Player p1, Player p2){
		p1.setResult(ResultEnum.WIN);
		p2.setResult(ResultEnum.LOSE);
	}

	private void p2Win(Player p1, Player p2){
		p1.setResult(ResultEnum.LOSE);
		p2.setResult(ResultEnum.WIN);
	}

	private void draw(Player p1, Player p2){
		p1.setResult(ResultEnum.DRAW);
		p2.setResult(ResultEnum.DRAW);
	}

	public ResultEnum getResult(String ipAddress){
		if(playersMap.containsKey(ipAddress)){
			ResultEnum result = playersMap.get(ipAddress).getResult();
			playersMap.get(ipAddress).setResult(null);
			return result;
		}

		return ResultEnum.NONE;
	}

	public void resetResults(){
		for(Player p : playersMap.values()){
			p.setResult(null);
		}
	}

	private void resetHands(){
		for(Player p : playersMap.values()){
			p.setHand(null);
		}
	}

	public void resetPlayer(String key){
		playersMap.remove(key);
	}

	public void resetAllPlayers(){
		playersMap.clear();
	}

	public void setMaxOfGames(int maxOfGames){
		if(0 < maxOfGames){
			this.maxOfGames = maxOfGames;
		}
	}

	public int getMaxOfGames(){
		return maxOfGames;
	}

	public boolean hasPlayer(String key){
		return playersMap.containsKey(key);
	}

	public Player getPlayer(String key){
		return playersMap.get(key);
	}

	public String toString(){
		StringBuilder str = new StringBuilder();
		for(String key : playersMap.keySet()){
			str.append("key: " + key);
			str.append(", ");
			str.append("Hand: ");
			str.append(playersMap.get(key).getHand());
			str.append(", ");
			str.append("Result: ");
			str.append(playersMap.get(key).getResult());
			str.append("\n");
		}
		return str.toString();
	}
}
