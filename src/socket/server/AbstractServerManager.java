package socket.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

abstract public class AbstractServerManager{

	private final int PORT = 20000;
	private int timeoutMsec =  5 * 60 * 1000;

	private ServerSocket ss = null;
	private Socket sc = null;

	private boolean continueServer = true;

	//	private WeakReference<SocketThread> wr;

	public AbstractServerManager(){
		try{
			ss = new ServerSocket(PORT);
			ss.setSoTimeout(timeoutMsec);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * サーバを開く
	 * @throws SocketTimeoutException タイムアウト時にスロー。ただしactionOfCatchSocketTimeoutExceptionの挙動に準ずる。
	 * @throws IOException
	 */
	final public void startServer() throws SocketTimeoutException, IOException{
		while(continueServer){
			try{
				//サーバ側ソケット
				System.out.println("Waiting...");
				this.sc = ss.accept(); //クライアントからの通信開始要求が来るまで待機
				System.out.println("Get Socket.");
				if(sc.isConnected()){
					new SocketThread(sc).start();
				}
			}catch(SocketTimeoutException ste){
				actionOfCatchSocketTimeoutException(ste);
			}catch(IOException e){
				e.printStackTrace();
				closeServer();
				throw e;
			}
		}
	}

	/**
	 * サーバを閉じる
	 */
	final public void closeServer(){
		continueServer = false;
		try{
			if(ss != null){
				ss.close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	protected void setTimeoutMsec(int msec){
		this.timeoutMsec = msec;
		if(ss != null){
			try{
				ss.setSoTimeout(timeoutMsec);
			}catch(SocketException e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * ソケットを受信したときのアクションを定義
	 * @param mes 受信メッセージ
	 * @param ipAddress 受信IPアドレス
	 * @return ソケット返信メッセージ
	 * @throws IOException
	 */
	protected abstract String actionOfReceivedSocketMessage(String mes, String ipAddress) throws IOException;

	protected abstract void actionOfCatchSocketTimeoutException(SocketTimeoutException e) throws SocketTimeoutException;

	/**
	 * 受信時に立てるスレッド用クラス
	 * @author Takashi
	 *
	 */
	private class SocketThread extends Thread{
		private Socket sc;

		public SocketThread(Socket sc) {
			this.sc = sc;
		}

		@Override
		final public void run(){
			try {

				String reqMes = receiveMessage(sc);
				String resMes = actionOfReceivedSocketMessage(reqMes, sc.getInetAddress().getHostAddress());
				sendResponse(sc, resMes);
				sc.close();

			}catch (IOException e){
				e.printStackTrace();
			}
		}

		final private String receiveMessage(final Socket sc) throws IOException{
			BufferedReader br = new BufferedReader(new InputStreamReader(sc.getInputStream()));
			System.out.print("Receive Reqest: ");
			String str = br.readLine();
			System.out.println(str);

			return str;
		}

		final private void sendResponse(final Socket sc, final String str) throws IOException{
			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sc.getOutputStream())));
			pw.println(str);
			pw.flush();
			System.out.println("Send Response: " + str);
		}
	}
}

